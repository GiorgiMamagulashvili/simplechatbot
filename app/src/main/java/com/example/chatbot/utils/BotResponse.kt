package com.example.chatbot.utils

import com.example.chatbot.utils.Constants.OPEN_GOOGLE
import com.example.chatbot.utils.Constants.OPEN_SEARCH

object BotResponse {

    fun basicResponse(_message: String): String {
        val random = (0..2).random()
        val message = _message.toLowerCase()

        return when {

            //Hello
            message.contains("hello") -> {
                when (random) {
                    0 -> "Hey"
                    1 -> "What's APPPP"
                    2 -> "Gamarjoba!!!"
                    else -> "error"
                }
            }
            //How are you
            message.contains("how are ") -> {
                when (random) {
                    0 -> "I'm Fine and You?"
                    1 -> "Thanks"
                    2 -> "Aramishavs"
                    else -> "error"
                }
            }
            //Arsenal Or Chelsea
            message.contains("arsenal or chelsea") -> {
                when (random) {
                    0 -> "Arsenal Of Course"
                    1 -> "London is Red"
                    2 -> "Arsenal"
                    else -> "error"
                }
            }

            message.contains("solve") -> {
                val equation: String = message.substringAfter("solve")
                return try {
                    val answer = SolveMath.solveMath(equation ?: "0")
                    answer.toString()
                } catch (e: Exception) {
                    "Sorry, Icant solve "
                }
            }

            //get current time
            message.contains("time") && message.contains("?") -> {
                Time.timeStamp()
            }
            message.contains("open") && message.contains("google") -> {
                OPEN_GOOGLE
            }
            message.contains("search") -> {
                OPEN_SEARCH
            }
            else -> {
                when (random) {
                    0 -> "idk"
                    1 -> "I dont understand"
                    2 -> "Hmmmmmmm"
                    else -> "error"
                }
            }
        }
    }
}