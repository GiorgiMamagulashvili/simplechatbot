package com.example.chatbot.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatbot.R
import com.example.chatbot.data.Message
import com.example.chatbot.utils.BotResponse
import com.example.chatbot.utils.Constants.OPEN_GOOGLE
import com.example.chatbot.utils.Constants.OPEN_SEARCH
import com.example.chatbot.utils.Constants.RECEIVE_ID
import com.example.chatbot.utils.Constants.SEND_ID
import com.example.chatbot.utils.Time
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: Adapter
    private val botList = listOf("Giorgi", "Aaron", "Tengo", "Jumber")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recycleView()
        clickEvent()


        val random = (0..3).random()
        customMessage("Hello You Are Speaking to ${botList[random]}")

    }
    private fun recycleView(){
        adapter = Adapter()
        chat_recycle.adapter = adapter
        chat_recycle.layoutManager = LinearLayoutManager(applicationContext)
    }
    private fun sendmessage(){
        val message = message_et.text.toString()
        val time =Time.timeStamp()
        if (message.isNotEmpty()){
            adapter.insertMessage(Message(message, SEND_ID,time))
            chat_recycle.scrollToPosition(adapter.itemCount-1)
            message_et.setText("")
            botResponse(message)

        }
    }

    private fun botResponse(message:String){
        val time = Time.timeStamp()

        GlobalScope.launch {
            delay(1000)

            withContext(Dispatchers.Main){
                val response = BotResponse.basicResponse(message)
                adapter.insertMessage(Message(response, RECEIVE_ID,time))
                chat_recycle.scrollToPosition(adapter.itemCount-1)

                when(response){
                    OPEN_GOOGLE->{
                        val site = Intent(Intent.ACTION_VIEW)
                        site.data  = Uri.parse("https://www.google.com/")
                        startActivity(site)
                    }
                    OPEN_SEARCH -> {
                        val site = Intent(Intent.ACTION_VIEW)
                        val searchTerm:String? = message.substringAfter("search")
                        site.data = Uri.parse("https://www.google.com/search?&q=$searchTerm")
                        startActivity(site)
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        GlobalScope.launch {
            delay(1000)
            withContext(Dispatchers.Main){
                chat_recycle.scrollToPosition(adapter.itemCount-1)
            }
        }
    }
    private fun clickEvent(){
        send_bt.setOnClickListener {
            sendmessage()
        }
        message_et.setOnClickListener {
            GlobalScope.launch {
                delay(1000)
                withContext(Dispatchers.Main){
                    chat_recycle.scrollToPosition(adapter.itemCount-1)
                }
            }
        }
    }

    private fun customMessage(message: String) {
        GlobalScope.launch {
            delay(1000)
            withContext(Dispatchers.Main) {
                val timeStap = Time.timeStamp()
                adapter.insertMessage(Message(message, RECEIVE_ID, timeStap))
                chat_recycle.scrollToPosition(adapter.itemCount - 1)

            }
        }
    }
}